package iutclf.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccmanagerApplication {
	public static void main(String[] args) {
		SpringApplication.run(AccmanagerApplication.class, args);
	}
}
