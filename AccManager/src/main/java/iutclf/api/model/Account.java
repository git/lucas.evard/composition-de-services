package iutclf.api.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class Account {
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "sommes")
    private double sommes;
    @Column(name = "risk")
    private String risk;

    public Account(double sommes,String risk){
        this.sommes=sommes;
        this.risk=risk;
    }

    public Account() {}

    public double getSommes() {
        return sommes;
    }

    public String getRisk() {
        return risk;
    }

    public void setRisk(String risk) {
        this.risk = risk;
    }

    public void setSommes(double sommes) {
        this.sommes = sommes;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
