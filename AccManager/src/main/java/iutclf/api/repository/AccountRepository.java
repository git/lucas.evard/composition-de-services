package iutclf.api.repository;


import iutclf.api.model.Account;
import jakarta.persistence.Entity;
import org.springframework.data.repository.CrudRepository;
import java.util.List;
import java.util.Optional;

public interface AccountRepository extends CrudRepository<Account,Long> {
    List<Account> findAll();
    Optional<Account> findById(Long id);
    void save(Entity account);
}
