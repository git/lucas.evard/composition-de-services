package iutclf.api.controllers;

import iutclf.api.model.Account;
import iutclf.api.repository.AccountRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class AccountsController {

    private final AccountRepository accountRepository;

    public AccountsController(AccountRepository accountRepository){
        this.accountRepository=accountRepository;
        this.accountRepository.save(new Account(500,"High"));
        this.accountRepository.save(new Account(500,"Low"));
        this.accountRepository.save(new Account(50000,"Low"));
        this.accountRepository.save(new Account(50000,"High"));
    }

    @GetMapping("/accounts")
    public @ResponseBody List<Account> getAccounts(){
        return accountRepository.findAll();
    }

    @PostMapping("/accounts/add")
    public @ResponseBody Account addAccount(@RequestBody Account account){
        accountRepository.save(account);
        return account;
    }

    @PutMapping("/accounts/{id}/{somme}")
    public @ResponseBody Account modifyAccount(@PathVariable Long id,@PathVariable double somme){
        for (Account account:getAccounts()) {
            if(account.getId().equals(id)){
                account.setSommes(account.getSommes()+somme);
                this.accountRepository.save(account);
                return account;
            }
        }
        throw new RuntimeException("Personne introuvable");
    }
    @GetMapping("/accounts/{id}")
    public @ResponseBody Optional<Account> getAccount(@PathVariable Long id){
        return accountRepository.findById(id);
    }
}
