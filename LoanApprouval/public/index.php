<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

$loader = require __DIR__ . '/../vendor/autoload.php';
//for namespace BL PSR4 here namespace ; dir
$loader->addPsr4('BL\\', __DIR__);
$app = AppFactory::create();


$app->get('/', function (Request $request, Response $response, $args) {
    $response->getBody()->write("Hello world!");
    return $response;
});

$app->get('/loan', function (Request $request, Response $response, $args) {
    $params=$request->getQueryParams();
    $account=$params["account"];
    $approval=$params["approval"];
    $somme=$params["somme"];
    $urlAccount = file_get_contents("http://localhost:8081/accounts/".$account);
    $urlApproval = file_get_contents("http://localhost:8080/approvals/".$approval);

    if($somme<10000){
        $data=(array)json_decode($urlAccount);
        if($data["risk"]=="High"){
           $response->getBody()->write("refused | risk=High et somme<10000");
        }
        else{
            $urlAccount=("http://localhost:8081/accounts/".$account."/".$somme);
            echo $urlAccount;
            $ch=curl_init($urlAccount);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch,CURLOPT_CUSTOMREQUEST,"PUT");
            $out=curl_exec($ch);
            curl_close($ch);
            $response->getBody()->write("accepted | somme<10000".$out);
        }
    }
    else{
        $data=(array)json_decode($urlApproval);
        if($data["accept"]){
            $urlAccount=("http://localhost:8081/accounts/".$account."/".$somme);
            echo $urlAccount;
            $ch=curl_init($urlAccount);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch,CURLOPT_CUSTOMREQUEST,"PUT");
            $out=curl_exec($ch);
            curl_close($ch);
            $response->getBody()->write("accepted | Approval déjà accepté".$out);
        }
        else{
            $response->getBody()->write("refused | Approval déjà refusé");
        }
    }
    return $response;
});


$app->run();
