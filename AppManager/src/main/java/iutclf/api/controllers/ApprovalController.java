package iutclf.api.controllers;

import iutclf.api.model.Approval;
import iutclf.api.repository.ApprovalRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ApprovalController {

    private final ApprovalRepository approvalRepository;

    public ApprovalController(ApprovalRepository approvalRepository){
        this.approvalRepository=approvalRepository;
        this.approvalRepository.save(new Approval());
        this.approvalRepository.save(new Approval(true));
    }

    @GetMapping("/approvals")
    public @ResponseBody List<Approval> getApprovals(){
        return approvalRepository.findAll();
    }

    @GetMapping("/approvals/{id}")
    public @ResponseBody Optional<Approval> getApproval(@PathVariable Long id){
        return approvalRepository.findById(id);
    }

    @DeleteMapping("/approvals/del/{id}")
    public void removeApproval(@PathVariable Long id){
        approvalRepository.deleteById(id);
    }

    @PostMapping("/approvals/add")
    public @ResponseBody Approval addApproval(@RequestBody Approval approval){
        return approvalRepository.save(approval);
    }

}
