package iutclf.api.repository;


import iutclf.api.model.Approval;
import org.springframework.data.repository.CrudRepository;

import javax.swing.text.html.parser.Entity;
import java.util.List;
import java.util.Optional;

public interface ApprovalRepository extends CrudRepository<Approval,Long> {
    List<Approval> findAll();
    Optional<Approval> findById(Long id);
    void save(Entity approval);
    void deleteById(Long entity);
}
