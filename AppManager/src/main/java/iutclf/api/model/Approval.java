package iutclf.api.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class Approval {
    @Id
    @GeneratedValue
    private Long id;

    private Boolean accept;

    public Approval(){
        this.accept=false;
    }

    public Approval(Boolean accept){
        this.accept=accept;
    }


    public Long getId() {
        return id;
    }

    public Boolean getAccept() {
        return accept;
    }
}
