# composition-de-service


Soit la composition suivante (écrite dans un langage informatique appelé BPEL pour information) qui permet de définir le service composite LoanApproval (gestion de prêts) :


![image](https://perso.limos.fr/~sesalva/files/webservice2A/tp/tp5/files/loan.png)


- Un premier service AccManager permet d’ajouter (add(account_id, somme, risk)), de lister des comptes bancaires accounts(), et récupérer l'état d'un compte accounts/account(account_id); 
- Un second service AppManager permet d’ajouter, de supprimer et de lister des approval (réponses de prêt, ce service simule un humain, routes approvals et approvals/approval/{id} par exemple),
- Enfin, LoanApproval est un service (celui de la Figure ci-dessus) qui reçoit des demandes de crédits loan(account_id, approval_id, somme). Si la somme est inférieure à 10000e, AccManager est appelé pour connaitre le risque sur le compte de la personne demandant le crédit. Si le risque est égal à "high", AppManager est appelé et retourne une réponse approved/refused. Sinon la réponse est approved. Si la somme est supérieure à 10000e ou si le risque est égal à "high", AppManager est appelé pour connaitre la réponse. La réponse est retournée à l’utilisateur avec l'état de son compte (si la réponse est approved, le compte est crédité et le message approved est renvoyé).